import os

os.chdir("/home/lil/Documents/499CSC/neurons")

import re

class SWC_Reader():
    def rf (self,fname):
        lines= []
        with open(fname,'r') as phile:
            lines = phile.readlines()
        return lines

    def line_by_line(self,lst):
        neuron_dct = {}
        for i in range(len(lst)):
            try:
                self_id = lst[i].split()[0]
                if str.isdigit(self_id):
                    start = [float(ele) for ele in lst[i].split()[-5:-2]]
                    parent_id = lst[i].split()[-1]
                    neuron_dct[self_id] = {'xyz':start,'parent':parent_id}
                else:
                    continue
            except:
                continue
        return neuron_dct

    def str_sort(self,val):
        return int(val)

    def make_obj(self,res_dict,fname):
        sorted_keys = sorted(res_dict.keys(),key=self.str_sort)
        line_entries = ""
        vertices = ""
        for v_key in sorted_keys:
            dct_entry = res_dict[v_key]
            xyz = str(dct_entry['xyz'])
            print(dct_entry)
            vertices += "v {} \n".format(re.sub(r'\[|\]|,',' ',xyz))
            line_entries+= "l {} {} \n".format( int(dct_entry['parent']),int(v_key))
        with open(fname,"w") as phile:
            phile.write(vertices)
            phile.write(line_entries)
     
    def run_full_process():
        if not os.path.exists(base):
            os.mkdir(base)
        for pth,subdir,fls in os.walk("./swcs"):
            if "swc" in " ".join(fls):
                for swc_path in fls:
                    swc_name = swc_path.split('/')[-1] 
                    res = self.line_by_line(self.rf(os.path.join(pth,swc_path)))
                    list_vertices(res,os.path.join(base,swc_name))


##[os.rename(os.path.join(base,ele), os.path.join(base,ele.replace('swc','obj'))) for ele in os.listdir(base)]


           




## y−y1=y2−y1x2−x1(x−x1) and z−z1=z2−z1x2−x1(x−x1)
def line_fmla(first,second):
    def fmla(self,x):
        return second[1]+ first[1] -first[1]*second[0] - first[0](x-first[0])
    return fmla

def pmet_fmla(first,second):
    dir_vec= [ele[1]-ele[0] for ele in zip(first,second)] 
    def fmla(self,t):
        return (first[0]+t*dir_vec[0],first[1]+t*dir_vec[1],first[2]+t*dir_vec[2])
    return fmla

def spit_out_steps(fmla,starting,end,steps):
    for i in range(steps):
        plus= (i/steps)*end
        print(fmla(starting + plus))
        


## could do parametric also which might be better at getting points just within space between

## create function taking one line and the next, moving through the list one at a time
def detailed_positions(neuron_pos_dct,resolution,fname):
        ##create file for output
    with open(fname,'w') as phile:
            ## step through dictionary
        for child_id in neuron_pos_dct:
                ## find parent each time, make parent start pt
                ## 
            print(child_id)
            child = neuron_pos_dct[child_id]
            if child['parent'] =='-1':
                print('hit parent')
                phile.write('0.0,0.0,0.0'+'\n')
                continue
            parent = neuron_pos_dct[child['parent']]
            line_fmla = pmet_fmla(parent['xyz'],child['xyz'])
            for t in range(resolution):
                pos = line_fmla(t/resolution)
                pos = str(pos).replace(')','').replace('(','')
                phile.write(pos+'\n')




#detailed_positions(line_by_line(rf("./purkinje.swc")),20,'detailed_points')
res = line_by_line(rf("./purkinje.swc"))
base = './neuron_obs'

## this is the blender stuff to import and setup the bevel before exporting
#import curv
import os
os.chdir('/home/lil/Documents/499CSC/neurons')
name = "14-8-3.CNG.obj"
names = os.listdir("./neuron_obs/")
for name in names:
    swc_obj = os.path.abspath(os.path.join(os.getcwd(),"neuron_obs",name))
    print(bpy.ops.import_scene.obj(filepath=swc_obj))

    curv = [item for key,item in C.scene.objects.items() if key in name][0]
    C.scene.objects.active = curv
    bpy.ops.object.convert(target = 'CURVE')


    #define tap
    tap = prev_tape

    #set its attributes
    curv.data.bevel_depth = .38
    curv.data.bevel_resolution = 3
    curv.data.fill_mode = 'FULL'
    curv.data.taper_object = tap

    ##export 
    out_name = "meshed"+ name
    out_path =  os.path.abspath(os.path.join(os.getcwd(),"neuron_obs",out_name))
    bpy.ops.object.convert(target = 'MESH')
    bpy.ops.export_scene.obj(filepath=out_path)
    ## then delet
    bpy.ops.mesh.delete(type='VERT')


import os
os.chdir('/home/lil/Documents/499CSC/neurons')
names = os.listdir("./neuron_obs/")
tap = C.scene.objects.active
for name in names:
    swc_obj = os.path.abspath(os.path.join(os.getcwd(),"neuron_obs",name))
    print(bpy.ops.import_scene.obj(filepath=swc_obj))
    curv = [item for key,item in C.scene.objects.items() if key in name][0]
    C.scene.objects.active = curv
    bpy.ops.object.convert(target = 'CURVE')
    #define tap
    #set its attributes
    curv.data.bevel_depth = .38
    curv.data.bevel_resolution = 3
    curv.data.fill_mode = 'FULL'
    curv.data.taper_object = tap
    ##export 
    out_name = "meshed"+ name
    out_path =  os.path.abspath(os.path.join(os.getcwd(),"neuron_obs",out_name))
    print(out_path)
    bpy.ops.object.convert(target = 'MESH')
    bpy.ops.export_scene.obj(filepath=out_path)
    ## then delet
    bpy.ops.object.delete()


