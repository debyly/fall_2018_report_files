import numpy as np
import matplotlib.pyplot as plt

class Catmull_object():
    def __init__(self):
        self.cat_mul_x = []
        self.cat_mul_z = []
        self.cat_mul_y = []

    def set_points(self,x,y,z,sparsity):
        if len(x)-1 > sparsity:
            #import pdb;pdb.set_trace()
                ## pluck out every sparsity'th value
            indices = np.r_[np.arange(0,len(x),sparsity),-1]
            self.x_points =np.array(x)[indices]
            self.y_points =np.array(y)[indices]
            self.z_points =np.array(z)[indices]
        else:
            self.x_points =np.array(x)
            self.y_points =np.array(y)
            self.z_points =np.array(z)
    def catmullize(self,points):
        points_array = points.reshape(4,1)
        big_mat = 1/2*(np.array('0 2 0 0 -1 0 1 0 2 -5 4 -1 -1 3 -3 1'.split()).reshape(4,4).astype('float32'))## matrix to convert from the system of 4 equations
        def point_generator(t):
            param_mat = np.array([1,t,t**2,t**3]).reshape(1,4)
            return np.matmul(np.matmul(param_mat,big_mat),points_array)
        return point_generator

    def add_first_last(self):
        ## adding a new first that's in line with the first two points
        o = np.array([self.x_points[0],self.y_points[0],self.z_points[0]])
        p = np.array([self.x_points[1],self.y_points[1],self.z_points[1]])
        for i in range(1,3):
            n = (i+1)*o - i*p
            self.x_points = np.insert(self.x_points,0,n[0])
            self.y_points = np.insert(self.y_points,0,n[1])
            self.z_points = np.insert(self.z_points,0,n[2])
            ## adding new last in line with the last two
        o = np.array([self.x_points[-1],self.y_points[-1],self.z_points[-1]])
        p = np.array([self.x_points[-2],self.y_points[-2],self.z_points[-2]])
        for i in range(2):
            n = (i+1)*o - i*p
            self.x_points = np.insert(self.x_points,-1,n[0])
            self.y_points = np.insert(self.y_points,-1,n[1])
            self.z_points = np.insert(self.z_points,-1,n[2])



    def calculate_lines(self):
        self.add_first_last()
        for i in range(1,self.x_points.size-4):
            x_subset = self.x_points[i:i+4]
            y_subset = self.y_points[i:i+4]
            z_subset = self.z_points[i:i+4]
            x_linefmla = self.catmullize(x_subset)
            y_linefmla = self.catmullize(y_subset)
            z_linefmla = self.catmullize(z_subset)
            for val in np.linspace(0,1,100):
                self.cat_mul_x.append( x_linefmla(val))
                self.cat_mul_y.append( y_linefmla(val))
                self.cat_mul_z.append( z_linefmla(val))
        #self.plot_arrays(self.cat_mul_x,self.cat_mul_y)

    def convert_lists(self):
        self.cat_mul_x =np.array([val for ele in self.cat_mul_x for val in ele]).flatten()
            ## must reshape before packing into same array
        self.cat_mul_y =  np.array([val for ele in self.cat_mul_y for val in ele]).flatten()
        self.cat_mul_z =  np.array([val for ele in self.cat_mul_z for val in ele]).flatten()

    def plot_arrays(self):
        if type(self.cat_mul_x == list):
            self.convert_lists()
        plt.scatter(self.cat_mul_x,self.cat_mul_y)
        #plt.plot(x_points,y_points)
        plt.show()

    def export_catmuls(self,fname,dim):
        if type(self.cat_mul_x) == list:
            self.convert_lists()
        if dim == 2:
            pair_arr =  np.c_[self.cat_mul_x,self.cat_mul_y] ## this is a column concat trick from numpy
        else:
            pair_arr =  np.c_[self.cat_mul_x,self.cat_mul_y,self.cat_mul_z] ## this is a column concat trick from numpy
        pair_arr.astype('float32').tofile(fname)





