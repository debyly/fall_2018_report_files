import catmullscript
import numpy as np
import os
import importlib
importlib.reload(catmullscript)

class SWC_Reader():
    def rf (self,fname):
        lines= []
        with open(fname,'r') as phile:
            lines = phile.readlines()
        return lines

    def line_by_line(self,lst):
        neuron_dct = {}
        for i in range(len(lst)):
            try:
                if str.isdigit(lst[i].split()[0]):
                    data = lst[i].split()
                    self_id = int(data[0])
                    color = int(data[1])
                    radius = float(data[-2])
                    start = [float(ele) for ele in data[-5:-2]]
                    parent_id = int(data[-1])
                    neuron_dct[self_id] = {
                            'xyz':start,
                            'parent':parent_id,
                            'radius':radius,
                            'color':color}
                else:
                    continue
            except:
                continue
        return neuron_dct

    def str_sort(self,val):
        return int(val)

    def make_obj(self,res_dict,fname):
        sorted_keys = sorted(res_dict.keys(),key=self.str_sort)
        line_entries = ""
        vertices = ""
        for v_key in sorted_keys:
            dct_entry = res_dict[v_key]
            xyz = str(dct_entry['xyz'])
            vertices += "v {} \n".format(re.sub(r'\[|\]|,',' ',xyz))
            line_entries+= "l {} {} \n".format( int(dct_entry['parent']),int(v_key))
        with open(fname,"w") as phile:
            phile.write(vertices)
            phile.write(line_entries)
     
    def run_full_process(self):
        for pth,subdir,fls in os.walk("./proc_dir"):
            if "swc" in " ".join(fls):
                for swc_path in fls:
                    swc_name = swc_path.split('/')[-1] 
                    #interesting_types = 'granule pyramidal spindle basket purkinje stellate golgi'.split(' ')
                    #if not( True in [srch_str in swc_name for srch_str in interesting_types]): 
                    self.export_helper(os.path.join(pth,swc_path),swc_name)

    def make_branches(self,filename,sparsity):
        neuron_dct = self.line_by_line(self.rf(filename))
        branches = []
        colors_list =[]
        radii_list = []
        tips = []
        for key,val in neuron_dct.items():
            parent= val['parent']
            if parent < key -1 :
                if key ==4:
                    tips.append(4)
                else:
                    tips.append(key-1)
        tips.remove(0)
        for tip in tips:
            branch =[]
            color_set = []
            radii_set = []
            present_id = tip
            present_info = neuron_dct[tip]
            branch.append(present_info['xyz']) ## have to double up so that no single points get stored
            color_set.append(present_info['color'])
            radii_set.append(present_info['radius'])
            while present_info['parent'] != -1:
                present_id = present_info['parent']
                present_info = neuron_dct[present_id]
                branch.append(present_info['xyz'])
                color_set.append(present_info['color'])
                radii_set.append(present_info['radius'])
            branches.append(branch)
            colors_list.append(color_set)
            radii_list.append(radii_set)
        branches
        ## break into separate axis collections
        point_cat = catmullscript.Catmull_object()
        color_rad_cat = catmullscript.Catmull_object()
        for point_branch,color_branch,rad_branch in zip(branches,colors_list,radii_list):

            x,y,z = [[],[],[]]
            for point in point_branch:
                x.append(point[0])
                y.append(point[1])
                z.append(point[2])
            #treat radius and color as x y z also
            color_rad_cat.set_points(color_branch,rad_branch,x,sparsity) # x is just filler here, won't be used necessarily
            color_rad_cat.calculate_lines()
            point_cat.set_points(x,y,z,sparsity)
            point_cat.calculate_lines()
        return (color_rad_cat,point_cat)

    def export_helper(self,swc_file,out_fname):
        (color_cat,point_cat) = self.make_branches(swc_file,4)

        color_cat.export_catmuls('{}col'.format(out_fname),2)
        point_cat.export_catmuls('{}points'.format(out_fname),3)

        pointsBin = np.fromfile('{}points'.format(out_fname),dtype='float32').reshape(-1,3)
        colorobin = np.fromfile('{}col'.format(out_fname),dtype='float32').reshape(-1,2)
        allbin = np.c_[pointsBin,colorobin]
        allbin.tofile('{}both'.format(out_fname))


